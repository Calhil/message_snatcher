package bartosz.snatcher.handlers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @param <I> input message type
 * @param <O> output message type
 */
public abstract class AbstractChainHandler<I, O> {

    protected final Logger log = LoggerFactory.getLogger(getClass());

    private AbstractChainHandler<O, ?> nextHandler;

    protected AbstractChainHandler() {
    }

    /**
     *
     * @param nextHandler next handler in chain
     * @param <NHO> next handler's output
     */
    public <NHO> AbstractChainHandler<O, NHO> link(AbstractChainHandler<O, NHO> nextHandler) {
        if (nextHandler == this) {
            throw new IllegalStateException("Cannot add self as next handler. It could cause circular dependency and/or infinite loop");
        }

        this.nextHandler = nextHandler;
        return nextHandler;
    }

    public abstract O process(I message) throws Exception;

    public abstract boolean shouldStop(I message);

    public void handle(I message) throws Exception {

        if (message == null) {
            return;
        }

        O processedMessage = null;
        try {
            processedMessage = process(message);
        } catch (Exception e) {
            log.error("Encountered exception while processing {}", message, e);
            throw e;
        }

        boolean isLastHandler = nextHandler == null;
        if (shouldStop(message) || isLastHandler) {
            log.debug("Stopping processing");
            return;
        }

        nextHandler.handle(processedMessage);
    }

}
