package bartosz.snatcher;

import bartosz.snatcher.adapters.ActiveMQMessageConsumerFactory;
import bartosz.snatcher.handlers.ByteMessageHandler;
import bartosz.snatcher.handlers.LoggingHandler;
import bartosz.snatcher.listeners.OrchestratingMessageListener;
import bartosz.snatcher.listeners.QueuingMessageListener;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jms.*;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

public class Snatcher {

    public static final Logger log = LoggerFactory.getLogger(Snatcher.class);
    private ExceptionListener exceptionListener;
    private OrchestratingMessageListener messageListener;

    public void start(String[] args) throws IOException, JMSException {
        // read config
        // create queue
        // subscribe to topics and place msgs on the queue
        // queue consumer
        // frontend (log)
        // frontend (cli)
        // frontend (spring+react)

        exceptionListener = new LoggingExceptionListener();
        messageListener = new OrchestratingMessageListener();
//        messageListener.add(new LoggingMessageListener("DEFAULT"));
        Queue<Message> queue = new LinkedBlockingQueue<>();
        messageListener.add(new QueuingMessageListener(queue));

        Properties config = readConfig("application.properties");

        ActiveMQMessageConsumerFactory activeMQMessageConsumerFactory = new ActiveMQMessageConsumerFactory(config);
        List<MessageConsumer> messageConsumers = activeMQMessageConsumerFactory.create(() -> messageListener);

        LoggingHandler<Message> loggingHandler = new LoggingHandler<>("Handler 1");
        loggingHandler
                .link(new ByteMessageHandler());
//                .link(new StringTransformingHandler())
//                .link(new LoggingHandler<String>("Handler 2"));

        Thread consumerThread = new Thread(new MessageProcessor<>(queue, loggingHandler));
        consumerThread.start();

//        DummyApplication.thread(new ConfigurableMessageProducer(config, "TEST.FOO", "MESSAGE 1"), false);
//        DummyApplication.thread(new ConfigurableMessageProducer(config, "TEST.FOO", "MESSAGE 2"), false);
//        DummyApplication.thread(new ConfigurableMessageProducer(config, "XXX", "MESSAGE 3"), false);
//        DummyApplication.thread(new ConfigurableMessageProducer(config, "YYY", "MESSAGE 4"), false);
    }

    private List<MessageConsumer> createMessageConsumers(Properties config) throws JMSException {

        Session session = createSession(config);

        String[] topics = ConfigUtils.stringArray(config, "app.topics");

        List<MessageConsumer> consumers = new ArrayList<>();
        for (String topic : topics) {
            log.info("Creating consumer for topic:: {}", topic);
            MessageConsumer consumer = createConsumer(session, topic);
            consumers.add(consumer);
            consumer.setMessageListener(messageListener);
        }

        return consumers;
    }

    private Session createSession(Properties config) throws JMSException {
        String brokerUrl = config.getProperty("app.brokerUrl");
        ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(brokerUrl);

        Connection connection = connectionFactory.createConnection();
        connection.start();
        connection.setExceptionListener(exceptionListener);

        return connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
    }

    private MessageConsumer createConsumer(Session session, String topic) throws JMSException {
        Destination destination = session.createTopic(topic);

        return session.createConsumer(destination);
    }

    private Properties readConfig(String filename) throws IOException {
        Properties properties = new Properties();

        try (InputStream fileStream = getClass().getClassLoader().getResourceAsStream(filename)) {
            properties.load(fileStream);
        }

        return properties;
    }

}
