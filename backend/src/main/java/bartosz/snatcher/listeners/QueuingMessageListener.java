package bartosz.snatcher.listeners;

import javax.jms.Message;
import javax.jms.MessageListener;
import java.util.Queue;

public class QueuingMessageListener implements MessageListener {

    private final Queue<Message> queue;

    public QueuingMessageListener(Queue<Message> queue) {
        this.queue = queue;
    }

    @Override
    public void onMessage(Message message) {
        queue.add(message);
    }
}
