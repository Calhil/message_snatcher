package bartosz.snatcher.listeners;

import javax.jms.Message;
import javax.jms.MessageListener;
import java.util.ArrayList;
import java.util.List;

public class OrchestratingMessageListener implements MessageListener {

    private final List<MessageListener> listeners;

    public OrchestratingMessageListener() {
        listeners = new ArrayList<>();
    }

    public void add(MessageListener messageListener) {
        listeners.add(messageListener);
    }

    @Override
    public void onMessage(Message message) {
        listeners.forEach(messageListener -> messageListener.onMessage(message));
    }
}
