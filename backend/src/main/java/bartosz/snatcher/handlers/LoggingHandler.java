package bartosz.snatcher.handlers;

public class LoggingHandler<T> extends AbstractChainHandler<T, T> {

    private final String name;

    public LoggingHandler(String name) {
        this.name = name;
    }

    @Override
    public T process(T message) {

        log.info("{} message:: {}", name, message);

        return message;
    }

    @Override
    public boolean shouldStop(T message) {
        return false;
    }


}
