package bartosz.snatcher.handlers;

import org.apache.activemq.command.ActiveMQBytesMessage;
import org.apache.logging.log4j.Logger;

import javax.jms.Message;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

import static org.apache.logging.log4j.LogManager.getLogger;

public class ByteMessageHandler extends AbstractChainHandler<Message, Message> {

    private static final Logger log = getLogger(ByteMessageHandler.class);

    private Path directory;

    public ByteMessageHandler() {
        Path newFile = null;
        try {
            Path byteMessages = Path.of("Byte Messages");
            directory = Files.createDirectory(byteMessages);
        } catch (IOException ignored) {
        }
    }


    @Override
    public Message process(Message message) throws Exception {

        if (message instanceof ActiveMQBytesMessage) {
            long bodyLength = ((ActiveMQBytesMessage) message).getBodyLength();

            byte[] buffer = new byte[(int) bodyLength];
            ((ActiveMQBytesMessage) message).readBytes(buffer);

            if (directory != null) {
                Path fileName = directory.resolve(message.getJMSMessageID());
                Path messageFilePath = Files.createFile(fileName);
                Files.write(messageFilePath, buffer, StandardOpenOption.APPEND);
            }

            log.info("Byte message length: {}, message: {}", bodyLength, buffer);

        }


        return null;
    }

    @Override
    public boolean shouldStop(Message message) {
        return false;
    }
}
