package bartosz.snatcher;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jms.MessageConsumer;
import java.util.Queue;

public class LoggingConsumer implements Runnable {

    public static final Logger log = LoggerFactory.getLogger(LoggingConsumer.class);
    private final Queue<String> queue;
    private final MessageConsumer consumer;

    private LoggingConsumer(MessageConsumer consumer, Queue<String> queue) {
        this.consumer = consumer;
        this.queue = queue;
    }

    public static LoggingConsumer of(MessageConsumer consumer, Queue<String> queue) {
        return new LoggingConsumer(consumer, queue);
    }

    @Override
    public void run() {
    }


}
