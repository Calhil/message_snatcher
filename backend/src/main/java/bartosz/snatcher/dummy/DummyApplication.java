package bartosz.snatcher.dummy;

public class DummyApplication {

    public static void main(String[] args) throws InterruptedException {
        thread(new DummyMessageProducer(), false);
        thread(new DummyMessageProducer(), false);
        thread(new DummyMessageConsumer(), false);
        Thread.sleep(1000);
        thread(new DummyMessageConsumer(), false);
        thread(new DummyMessageProducer(), false);
        thread(new DummyMessageConsumer(), false);
        thread(new DummyMessageProducer(), false);
        Thread.sleep(1000);
        thread(new DummyMessageConsumer(), false);
        thread(new DummyMessageProducer(), false);
        thread(new DummyMessageConsumer(), false);
        thread(new DummyMessageConsumer(), false);
        thread(new DummyMessageProducer(), false);
        thread(new DummyMessageProducer(), false);
        Thread.sleep(1000);
        thread(new DummyMessageProducer(), false);
        thread(new DummyMessageConsumer(), false);
        thread(new DummyMessageConsumer(), false);
        thread(new DummyMessageProducer(), false);
        thread(new DummyMessageConsumer(), false);
        thread(new DummyMessageProducer(), false);
        thread(new DummyMessageConsumer(), false);
        thread(new DummyMessageProducer(), false);
        thread(new DummyMessageConsumer(), false);
        thread(new DummyMessageConsumer(), false);
        thread(new DummyMessageProducer(), false);
    }

    public static void thread(Runnable runnable, boolean daemon) {
        Thread brokerThread = new Thread(runnable);
        brokerThread.setDaemon(daemon);
        brokerThread.start();
    }
}
