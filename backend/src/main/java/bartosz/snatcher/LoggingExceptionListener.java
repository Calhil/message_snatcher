package bartosz.snatcher;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jms.ExceptionListener;
import javax.jms.JMSException;

public class LoggingExceptionListener implements ExceptionListener {

    public static final Logger log = LoggerFactory.getLogger(LoggingExceptionListener.class);

    @Override
    public void onException(JMSException exception) {
        log.error("Encountered exception", exception);
    }
}
