package bartosz.snatcher.listeners;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jms.Message;
import javax.jms.MessageListener;

public class LoggingMessageListener implements MessageListener {

    public static final Logger log = LoggerFactory.getLogger(LoggingMessageListener.class);
    private final String name;

    public LoggingMessageListener(String name) {
        this.name = name;
    }

    @Override
    public void onMessage(Message message) {
        log.info("{}: message:: {}", name, message);
    }
}
