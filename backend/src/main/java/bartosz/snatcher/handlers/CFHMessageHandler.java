package bartosz.snatcher.handlers;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.activemq.command.ActiveMQBytesMessage;

import javax.jms.BytesMessage;
import javax.jms.Message;

public class CFHMessageHandler extends AbstractChainHandler<Message, String> {

    private final ObjectMapper objectMapper;

    public CFHMessageHandler(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @Override
    public String process(Message message) throws Exception {

        if (message instanceof BytesMessage) {

//            ((ActiveMQBytesMessage) message).
//            ((BytesMessage) message).

//            objectMapper.


            return "";

        } else {
            throw new IllegalStateException("Attempting handling unsupported message type:: " + message);
        }
    }

    @Override
    public boolean shouldStop(Message message) {
        return false;
    }
}
