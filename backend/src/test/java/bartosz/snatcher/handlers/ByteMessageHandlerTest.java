package bartosz.snatcher.handlers;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ByteMessageHandlerTest {

    public static final Logger log = LoggerFactory.getLogger(ByteMessageHandlerTest.class);

    @BeforeAll
    static void beforeAll() throws URISyntaxException, IOException {
        URI byteMessages = ByteMessageHandler.class.getClassLoader().getResource("byte_messages").toURI();
        Path path = Path.of(byteMessages);

        byte[] bytes = Files.readAllBytes(path);
//        List<String> strings = Files.readAllLines(path);
        log.info("byteMessages lines number: {}", bytes.length);
    }

    @Test
    void name() {
        //
    }
}
