package bartosz.snatcher.handlers;

import javax.jms.Message;
import javax.jms.TextMessage;

public class StringTransformingHandler extends AbstractChainHandler<Message, String> {

    @Override
    public String process(Message message) throws Exception {

        return ((TextMessage) message).getText();
    }

    @Override
    public boolean shouldStop(Message message) {
        return false;
    }
}
