package bartosz.snatcher;

import bartosz.snatcher.handlers.AbstractChainHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jms.Message;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;

/**
 *
 * @param <M> input message type from queue
 */
public class MessageProcessor<M> implements Runnable {

    public static final Logger log = LoggerFactory.getLogger(MessageProcessor.class);

    private final Queue<M> queue;
    private final AbstractChainHandler<M, ?> handlers;

    public MessageProcessor(Queue<M> queue, AbstractChainHandler<M, ?> handlerChain) {
        this.queue = queue;
        this.handlers = handlerChain;
    }

    // TODO 3/11/22: add config -> different frontends

    @Override
    public void run() {
        while (true) {
            try {

                if (queue instanceof LinkedBlockingDeque) {
                    M message = ((LinkedBlockingDeque<M>) queue).poll(100, TimeUnit.MILLISECONDS);
                    handlers.handle(message);
                } else {
                    M message = queue.poll();
                    handlers.handle(message);
                    TimeUnit.MILLISECONDS.sleep(100);
                }
            } catch (Exception e) {
                log.error("", e);
            }
        }

    }
}
