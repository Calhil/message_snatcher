package bartosz.snatcher;

import java.util.Properties;

public class ConfigUtils {

    public static final String DELIMITER = "\\s*,\\s*";

    private ConfigUtils() {
    }

    public static String emptyIfNull(String value) {
        return value == null ? "" : value;
    }

    public static String[] stringArray(Properties config, String propertyName) {

        return emptyIfNull(config.getProperty(propertyName)).split(DELIMITER);
    }
}
