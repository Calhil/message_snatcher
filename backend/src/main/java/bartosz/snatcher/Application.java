package bartosz.snatcher;

import javax.jms.JMSException;
import java.io.IOException;

public class Application {

    public static void main(String[] args) throws IOException, JMSException, InterruptedException {
        Snatcher app = new Snatcher();
        app.start(args);
    }

}
