package bartosz.snatcher.adapters;

import bartosz.snatcher.ConfigUtils;
import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.advisory.DestinationSource;
import org.apache.activemq.command.ActiveMQTopic;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jms.*;
import java.lang.IllegalStateException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.function.Supplier;

public class ActiveMQMessageConsumerFactory {

    public static final Logger log = LoggerFactory.getLogger(ActiveMQMessageConsumerFactory.class);

    private final Properties config;

    public ActiveMQMessageConsumerFactory(Properties config) {
        this.config = config;
    }

    public List<MessageConsumer> create(Supplier<MessageListener> messageListenerSupplier) throws JMSException {

        String[] topics = ConfigUtils.stringArray(config, "app.topics");

        List<MessageConsumer> messageConsumers = new ArrayList<>();

        if (StringUtils.isAllBlank(topics)) {
            // get topics from broker
            String[] brokerUrls = ConfigUtils.stringArray(config, "app.brokerUrl");

            if (StringUtils.isAllBlank(brokerUrls)) {
                throw new IllegalStateException("You have to specify either topics and/or a list of broker Urls");
            }

            for (String brokerUrl : brokerUrls) {
                ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory(brokerUrl);
                Connection connection = activeMQConnectionFactory.createConnection();
                connection.start();
                Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

                DestinationSource destinationSource = ((ActiveMQConnection) connection).getDestinationSource();
                for (ActiveMQTopic topic : destinationSource.getTopics()) {

                    log.info("Creating message consumer for broker {} on topic {}", brokerUrl, topic.getTopicName());

                    MessageConsumer consumer = session.createConsumer(topic);
                    consumer.setMessageListener(messageListenerSupplier.get());
                    messageConsumers.add(consumer);
                }
            }


        } else {
            // configuired topics
            throw new UnsupportedOperationException("Not supported");
        }


        return messageConsumers;
    }

}
