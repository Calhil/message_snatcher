package bartosz.snatcher.dummy;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.ActiveMQSession;
import org.apache.activemq.ActiveMQTopicSubscriber;
import org.apache.activemq.advisory.DestinationSource;
import org.apache.activemq.command.ActiveMQDestination;
import org.apache.activemq.command.ActiveMQTopic;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jms.*;
import java.util.Properties;

public class ConfigurableMessageProducer implements Runnable {

    public static final Logger log = LoggerFactory.getLogger(ConfigurableMessageProducer.class);
    private final String brokerUrl;
    private final String message;
    private final String topic;

    public ConfigurableMessageProducer(Properties config, String topic, String message) {
        brokerUrl = config.getProperty("app.brokerUrl");
        this.message = message;
        this.topic = topic;
    }


    @Override
    public void run() {
        try {
            // Create a ConnectionFactory
            ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(brokerUrl);

            // Create a Connection
            Connection connection = connectionFactory.createConnection();
            connection.start();

            // Create a Session
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

            // Create the destination (Topic or Queue)
//            Destination destination = session.createQueue("TEST.FOO");
            Destination destination = session.createTopic(topic);

            // Create a MessageProducer from the Session to the Topic or Queue
            MessageProducer producer = session.createProducer(destination);
            producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);

            // Create a messages
            String text = Thread.currentThread().getName() + " : " + message;
            TextMessage message = session.createTextMessage(text);

            // Tell the producer to send the message
            log.info("Sent message: "+ message.hashCode() + " : " + Thread.currentThread().getName());
            producer.send(message);

            // Clean up
            session.close();
            connection.close();
        }
        catch (Exception e) {
            log.error("Caught: " + e);
            e.printStackTrace();
        }

    }
}
